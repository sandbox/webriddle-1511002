   <div id="wrapper">
   	<div id="header">
   		
        <?php if ($logo): ?>
      	<h1>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        	<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
     	 </a>
         </h1>
    	<?php endif; ?>
       
      
         <div class="main-menu">
            <?php if ($primary_nav): print $primary_nav; endif; ?>
        		<?php if ($secondary_nav): print $secondary_nav; endif; ?>
         </div><!-- /main-menu -->
    	
	</div><!-- /header -->
    <div id="main">
    	<div class="sub-menu">
			<?php if ($page['header']): ?>
              <?php print render($page['header']); ?>
            <?php endif; ?>
        </div><!-- /sub-menu -->
        <div class="highlighted">
        	<?php print render($page['highlighted']); ?>
        </div><!-- /highlighted -->
   		<?php if ($page['sidebar_left']): ?> 
        <div class="sidebar-left">
              <?php print render($page['sidebar_left']); ?>
        </div><!-- /left-sidebar -->
        <?php endif; ?>
        <div class="content">
		<?php 
		//print render($breadcrumb); 
		?>
       <?php if ($title && !$is_front): ?>
        <h2><?php print $title; ?></h2>
      <?php endif; ?>
    
      <?php if ($tabs): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      
     <?php if ($messages): ?>
        <?php print $messages; ?>
     <?php endif; ?>
      
      
      <?php print render($page['content']); ?>
      </div><!-- /content-->
			 <?php if ($page['sidebar_right']): ?> 
         <div class="sidebar-right"> 
            <?php print render($page['sidebar_right']); ?>
         </div><!-- /left-sidebar -->
          <?php endif; ?>
        
   	</div><!-- /main -->
   	</div><!-- /wrapper -->
    <div id="footer">
		<?php if ($page['footer']): ?>
            <div class="footer-content">
                <?php print render($page['footer']); ?>
 			</div><!-- /footer-content -->
        <?php endif; ?>
   	</div><!-- /footer -->
   