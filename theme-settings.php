<?php
/**
* @file
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/

function unimarket_form_system_theme_settings_alter(&$form, $form_state) {
   $form['unimarket_inline'] = array(
    '#type' => 'radios',
    '#title' => t('Inline image'),
	'#options' => array(
		'yes_inline' => t('Inline'),
		'no_inline' => t('No inline'),
    ),
    '#default_value' => 'yes_inline',
    '#description' => t('Set node images to be inline'),
    '#weight' => -2,
  );
}
